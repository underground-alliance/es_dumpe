# -*- coding: utf-8 -*-
"""
 @Time : 2022/6/28 16:46
 @Author : xu
 @Version：V 0.1
 @File : export_data.py
 导出es，json结构到structure，csv到data
"""
import os
import csv
import time
import json
import requests
import pandas as pd
from elasticsearch import Elasticsearch, helpers

# 根据es连接，和索引名scroll查询全部
def es_iterate_all_documents(es, index, pagesize=250, scroll_timeout="1m", **kwargs):
    es_list = []
    """
    Helper to iterate ALL values from a single index
    Yields all the documents.
    """
    is_first = True
    while True:
        # Scroll next
        if is_first: # Initialize scroll
            result = es.search(index=index, scroll="1m", **kwargs, body={
                "size": pagesize
            })
            is_first = False
        else:
            result = es.scroll(body={
                "scroll_id": scroll_id,
                "scroll": scroll_timeout
            })
        scroll_id = result["_scroll_id"]
        hits = result["hits"]["hits"]
        # Stop after no more docs
        if not hits:
            break
        # Yield each entry
        es_list += hits
    return es_list

# 通过请求es获取es的所有节点和setting和mapping信息,并生成到
def gen_es_json_structure(key, jsonFilePath):
    # 通过索引名请求到索引的结构信息
    req_url = 'http://' + es_ip + ':9200/' + key
    resp = requests.get(req_url)
    req_json = resp.json()[key]
    mappings = req_json['mappings']
    settings = req_json['settings']
    if 'aliases' in req_json:
        del req_json['aliases']
    if 'creation_date' in settings['index']:
        del settings['index']['creation_date']
    if 'uuid' in settings['index']:
        del settings['index']['uuid']
    if 'version' in settings['index']:
        del settings['index']['version']
    fileName = '{}.json'.format(key)
    print('生成的文件名是 = ' + fileName)
    with open(jsonFilePath + fileName, 'w') as write_f:
        json.dump(req_json, write_f, indent=4, ensure_ascii=False)


# 将索引名为json文件名，setting+mapping拼接成json
def gen_es_csv_data(es,key, csvFilePath):
    data_list = es_iterate_all_documents(es, key)
    fileName = key + '.csv'
    with open(csvFilePath + fileName, 'a', newline='', encoding='utf_8_sig') as f:
        if len(data_list) > 0:
            writer = csv.DictWriter(f, fieldnames = ['_index','_type','_id','_score','_source'])
            writer.writeheader()
            writer.writerows(data_list)
            print('生成的文件名是 = ' + fileName)

def main(es_ip, jsonFilePath,csvFilePath):
    es = Elasticsearch(es_ip, timeout=360)
    alias = es.indices.get_alias()
    for key in alias.keys():
        # 根据索引名生成索引创建信息的json文件
        gen_es_json_structure(key, jsonFilePath)
        # 根据索引生成索引数据的csv文件
        gen_es_csv_data(es,key, csvFilePath)


if __name__ == '__main__':
    jsonFilePath = "../structure/"
    csvFilePath = "../data/"
    es_ip = input('输入es的ip,示例=127.0.0.1 请输入：')
    main(es_ip, jsonFilePath,csvFilePath)
