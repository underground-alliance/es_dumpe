#-*- coding: utf-8 -*-
"""
 @Time : 2022/6/28 16:47
 @Author : xu
 @Version：V 0.1
 @File : import_data.py
 导入es，json结构 + csv数据
"""
import os
import json
import requests
import pandas as pd
from elasticsearch import Elasticsearch, helpers

# 获取文件列表
def listdir(path,type): #传入根目录
    file_list = []
    for file in os.listdir(path):
        file_path = os.path.join(path, file) #获取绝对路径
        if os.path.splitext(file_path)[1] == type: #判断文件是否是json文件
            file_list.append(file)
    return file_list #返回Excel文件路径列表

# 数据批量插入es
def bulk_data(es,index_name, index_type, data_dict):
    actions = []
    for li in data_dict:
        json_data = eval(li['_source'])
        action={
        "_index": index_name,
        "_type": index_type,
        "_source": json_data
        }
        actions.append(action)
        if(len(actions) == 500):
            helpers.bulk(es, actions)
            print('分批次批量插入500 索引名：{}'.format(index_name))
            del actions[0:len(actions)]
    if (len(actions) > 0):
        helpers.bulk(es, actions)
        print('分批次批量插入剩余 索引名：{}'.format(index_name))


# 根据csv文件名存储到对应索引中
def import_data(es, csvFilePath):
    csv_list = listdir(csvFilePath, '.csv')
    for li in csv_list:
        data = pd.read_csv(csvFilePath + li, encoding='utf-8')
        index_name = li[:-4]
        data_dict = data.to_dict('records')
        index_type = data_dict[0]['_type']
        print('需导入的索引名：{} 类型：{} 数据列表行数是：{}'.format(index_name,index_type,data.shape[0]))
        bulk_data(es,index_name,index_type,data_dict)


# 根据es_ip连接到需要导入数据的es节点,遍历创建索引的json文件，之后导入csv数据文件
def main(es_ip, jsonFilePath,csvFilePath):
    es = Elasticsearch(es_ip, timeout=360)
    # 根据json文件创建索引
    import_structure(es, jsonFilePath)
    # 根据csv文件插入指定索引
    import_data(es, csvFilePath)


# 指定json文件创建索引
def import_structure(es, jsonFilePath):
    json_list = listdir(jsonFilePath, '.json')
    for li in json_list:
        index_info = json.load(open(jsonFilePath + li, 'r', encoding="utf-8"))
        # 根据读取到的json文件创建es索引并设置索引信息
        index_name = li[:-5]
        print('需要创建的索引名：{},创建时使用的索引信息：{}'.format(index_name, index_info))
        if not es.indices.exists(index=index_name):
            res = es.indices.create(index=index_name, body=index_info)
            print('创建 {} 索引的响应信息是：{}'.format(index_name, res))


if __name__ == '__main__':
    es_ip = '192.168.85.143'
    jsonFilePath = "../structure/"
    csvFilePath = "../data/"
    main(es_ip, jsonFilePath,csvFilePath)


