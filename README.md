# es_dump

#### 介绍
es导出结构到json，导出数据到csv中，完全copyES

#### 软件架构
es_handle目录为脚本目录
脚本执行进行导出的目录为，es_handle同级目录的data,structure文件夹


#### 使用说明
1.  pip install -r requirements.txt
2.  首先运行导出项目，将es的结构和数据分别导出到json和csv中
3.  之后使用导入脚本读取即可

### 注意事项
暂时只适应了es7以下版本
需要有对选中文件夹的读写权限
存放json和csv文件夹中不该有多级目录

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
